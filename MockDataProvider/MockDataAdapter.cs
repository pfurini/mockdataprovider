using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Data;
using System.Data.Common;

using MockDataProvider.Cache;

namespace MockDataProvider
{
    public class MockDataAdapter : DbDataAdapter
    {
        #region Private Vars

        private DbDataAdapter _dbDataAdapter = RealProviderFactory.Instance.CreateDataAdapter( );

        #endregion
        
        #region Constructors

        public MockDataAdapter( )
        {
            _dbDataAdapter.FillError += new FillErrorEventHandler( _dbDataAdapter_FillError );
        }

        #endregion

        #region Event Handlers

        private void _dbDataAdapter_FillError( object sender, FillErrorEventArgs e )
        {
            base.OnFillError( e );            
        }

        #endregion
    }
}