﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyTitle( "MockDataProvider" )]
[assembly: AssemblyDescription( "Mock ADO.NET Data Provider" )]

[assembly: ComVisible( false )]

[assembly: Guid( "90e56c3e-cf5e-44ba-b263-3298d9c02444" )]

[assembly: CLSCompliant( true )]

[assembly: EnvironmentPermissionAttribute( SecurityAction.RequestMinimum, Read = "COMPUTERNAME;USERNAME;USERDOMAIN" )]

[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "Lagash Systems" )]
[assembly: AssemblyProduct( "Mock ADO.NET Data Provider" )]
[assembly: AssemblyCopyright( "Rodolfo Finochietti" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

[assembly: AssemblyVersion( "1.0.5.0" )]
[assembly: AssemblyFileVersion( "1.0.5.0" )]
[assembly: AssemblyInformationalVersion( "1.0.5.0" )]
