using System;
using System.Diagnostics;
using System.Globalization;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.IO;
using System.Xml.Serialization;

using MockDataProvider.Configuration;

namespace MockDataProvider.Cache
{
    internal class ResultCache
    {
        #region Factory

        public static readonly ResultCache Instance = new ResultCache( );
        
        #endregion

        #region Enums

        public enum CacheEntryType
        {
            ExecuteDbDataReader = 0,
            ExecuteScalar = 1,
            ExecuteNonQuery = 2
        }

        #endregion

        #region Constants

        private const string CacheKeySeparator = "|";

        #endregion
        
        #region Private vars

        private Dictionary<int, ResultCacheEntry> _cache = new Dictionary<int, ResultCacheEntry>( );

        private readonly object _syncRoot = new object( );
        private static TraceSource _logger;

        #endregion

        #region Properties

        public ResultCacheEntry this[ int hashedKey ]
        {
            get
            {
                return Get( hashedKey );
            }
        }

        private static TraceSource Logger
        {
            get { return _logger ?? (_logger = LogFactory.NewTraceSource()); }
        }

        #endregion

        #region Constructors
        private ResultCache( )
        {
            Load( );
        }

        #endregion
        
        #region Public Methods

        public ResultCacheEntry Get( int hashedKey )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "Get");
            if (!ConfigurationHelper.Instance.MockMode)
                return null;
            lock (_syncRoot)
            {

                if (_cache.ContainsKey(hashedKey))
                    return _cache[hashedKey];
            }
            Logger.TraceEvent(TraceEventType.Error, 0, "The result is not found in cache");
            throw new Exception( "The result is not found in cache" );
        }

        public void Add( int hashedKey, object scalarResult, string commandText, string parametersList )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "Add");
            if (ConfigurationHelper.Instance.MockMode)
                return;

            lock (_syncRoot)
            {
                if (!_cache.ContainsKey(hashedKey))
                    _cache.Add(hashedKey, new ResultCacheEntry(hashedKey, scalarResult, commandText, parametersList));
                else
                    _cache[hashedKey] = new ResultCacheEntry(hashedKey, scalarResult, commandText, parametersList);

                Save();
            }
        }

        public void Add( int hashedKey, object scalarResult, DataSet dataSetResult, string commandText, string parametersList )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "Add");
            if (ConfigurationHelper.Instance.MockMode)
                return;
            lock (_syncRoot)
            {

                if (!_cache.ContainsKey(hashedKey))
                    _cache.Add(hashedKey,
                               new ResultCacheEntry(hashedKey, scalarResult, dataSetResult, commandText, parametersList));
                else
                    _cache[hashedKey] = new ResultCacheEntry(hashedKey, scalarResult, dataSetResult, commandText,
                                                             parametersList);

                Save();
            }
        }

        public static int GetHashedKey( string cacheEntryType, DbCommand command )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "GetHashedKey");
            var sb = new StringBuilder();

            GetParameterDataString( sb, command );

            return ( String.Format( CultureInfo.InvariantCulture, "{0}{1}", cacheEntryType, sb ) ).GetHashCode( );
        }

        #endregion

        #region Private Methods

        private void Load( )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "Load");

            TextReader reader = null;
            try
            {
                if ( File.Exists( ConfigurationHelper.Instance.CacheFile ) )
                {
                    reader = new StreamReader( ConfigurationHelper.Instance.CacheFile );
                    var serializer = new XmlSerializer( typeof( ResultCacheEntry[ ] ) );
                    var cacheEntrys = ( ResultCacheEntry[ ] )serializer.Deserialize( reader );
                    reader.Close( );

                    _cache.Clear( );

                    foreach ( var entry in cacheEntrys )
                        _cache.Add( entry.HashedKey, entry );
                }
            }
            catch (Exception exc)
            {
                Logger.TraceData(TraceEventType.Error, 0, exc);

                if ( reader != null )
                    reader.Close( );

                if ( File.Exists( ConfigurationHelper.Instance.CacheFile ) )
                    File.Delete( ConfigurationHelper.Instance.CacheFile );
            }
        }
        
        private void Save( )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "Save");

            var cacheEntrys = new ResultCacheEntry[_cache.Count];

            var i = 0;
            foreach ( KeyValuePair<int, ResultCacheEntry> kvp in _cache )
            {
                cacheEntrys[ i ] = kvp.Value;
                i++;
            }

            TextWriter writer = new StreamWriter( ConfigurationHelper.Instance.CacheFile, false );
            var serializer = new XmlSerializer( typeof( ResultCacheEntry[ ] ) );
            serializer.Serialize( writer, cacheEntrys );
            writer.Close( );
        }

        private static void GetParameterDataString( StringBuilder sb, DbCommand command )
        {
            Logger.TraceData(TraceEventType.Start, 1001, "GetParameterDataString");

            if (command != null && !String.IsNullOrEmpty(command.CommandText))
            {
                sb.Append( CacheKeySeparator );
                sb.Append( command.CommandText );
                if ( command.Parameters != null && command.Parameters.Count != 0 )
                {
                    foreach ( DbParameter param in command.Parameters )
                    {
                        if ( param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            if ( !String.IsNullOrEmpty( param.ParameterName ) )
                            {
                                sb.Append( CacheKeySeparator );
                                sb.Append( param.ParameterName );
                            }

                            try
                            {
                                if (param.Value != null
                                    && (String.IsNullOrEmpty(param.ParameterName) ||
                                        !ConfigurationHelper.Instance.ParametersToIgnore.Contains(param.ParameterName))
                                    && (!ConfigurationHelper.Instance.IgnoreDateTimeFields ||
                                        (param.DbType != DbType.Date
                                         && param.DbType != DbType.DateTime
                                         && param.DbType != DbType.DateTime2
                                         && param.DbType != DbType.Time)))
                                {
                                    sb.Append(CacheKeySeparator);
                                    sb.Append(param.Value);
                                }
                            }
                            catch (Exception exc)
                            {
                                Logger.TraceData(TraceEventType.Error, 0, exc);

                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}