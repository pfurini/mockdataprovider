using System;
using System.Data;

namespace MockDataProvider.Cache
{
    [Serializable]
    public class ResultCacheEntry
    {
        #region Constructors

        public ResultCacheEntry()
        {
            
        }

        public ResultCacheEntry( int hashedKey, object scalarResult, string commandText, string parametersList)
        {
            _hashedKey = hashedKey;
            _scalarResult = scalarResult;
            _commandText = commandText;
            _parametersList = parametersList;
        }

        public ResultCacheEntry(int hashedKey, object scalarResult, DataSet dataSetResult, string commandText, string parametersList)
        {
            _hashedKey = hashedKey;
            _scalarResult = scalarResult;
            _dataSetResult = dataSetResult;
            _commandText = commandText;
            _parametersList = parametersList;
        }

        #endregion

        #region Properties

        private int _hashedKey;

        public int HashedKey
        {
            get
            {
                return _hashedKey;
            }
            set
            {
                _hashedKey = value;
            }
        }

        private object _scalarResult;

        public object ScalarResult
        {
            get
            {
                return _scalarResult;
            }
            set
            {
                _scalarResult = value;
            }
        }

        private DataSet _dataSetResult;

        public DataSet DataSetResult
        {
            get
            {
                return _dataSetResult;
            }
            set
            {
                _dataSetResult = value;
            }
        }

        private string _commandText;

        public string CommandText
        {
            get { return _commandText; }
            set { _commandText = value; }
        }


        private string _parametersList;

        public string ParametersList
        {
            get { return _parametersList; }
            set { _parametersList = value; }
        }

        #endregion
    }
}