using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;
using MockDataProvider.Cache;
using MockDataProvider.Configuration;

namespace MockDataProvider
{
    public class MockCommand : DbCommand
    {
        #region Private Vars
        
        private readonly DbCommand _dbCommand = RealProviderFactory.Instance.CreateCommand( );
        private DbTransaction _dbTransaction;

        private static TraceSource _logger;
        #endregion

        #region Constructors

        #endregion

        #region Private Vars

        private static TraceSource Logger
        {
            get { return _logger ?? (_logger = LogFactory.NewTraceSource()); }
        }

        [DefaultValue( "" )]
        [RefreshProperties( RefreshProperties.All )]
        public override string CommandText
        {
            get
            {
                return _dbCommand.CommandText;
            }
            set
            {
                _dbCommand.CommandText = value;
            }
        }

        public override int CommandTimeout
        {
            get
            {
                return _dbCommand.CommandTimeout;
            }
            set
            {
                _dbCommand.CommandTimeout = value;
            }
        }

        [RefreshProperties( RefreshProperties.All )]
        public override CommandType CommandType
        {
            get
            {
                return _dbCommand.CommandType;
            }
            set
            {
                _dbCommand.CommandType = value;
            }
        }

        [EditorBrowsable( EditorBrowsableState.Never )]
        [Browsable( false )]
        [DefaultValue( true )]
        [DesignOnly( true )]
        public override bool DesignTimeVisible
        {
            get
            {
                return _dbCommand.DesignTimeVisible;
            }
            set
            {
                _dbCommand.DesignTimeVisible = value;
            }
        }

        public override UpdateRowSource UpdatedRowSource
        {
            get
            {
                return _dbCommand.UpdatedRowSource;
            }
            set
            {
                _dbCommand.UpdatedRowSource = value;
            }
        }

        #endregion

        #region Public Methods

        public override void Cancel( )
        {
            _dbCommand.Cancel( );
        }

        public override int ExecuteNonQuery( )
        {
            int result;

            var hashedKey = ResultCache.GetHashedKey( ResultCache.CacheEntryType.ExecuteNonQuery.ToString( ), _dbCommand );
            var entry = ResultCache.Instance[ hashedKey ];
            if ( entry == null )
            {
                result = _dbCommand.ExecuteNonQuery( );
                Logger.TraceInformation("ExecuteNonQuery(): {0} --> result: {1}", _dbCommand.CommandText, result);
                ResultCache.Instance.Add(hashedKey, result, _dbCommand.CommandText, DbParameterCollectionToString(_dbCommand.Parameters));
            }
            else
            {
                result = ( int )entry.ScalarResult;
            }

            return result;
        }

        public override object ExecuteScalar( )
        {
            object result;

            var hashedKey = ResultCache.GetHashedKey( ResultCache.CacheEntryType.ExecuteScalar.ToString( ), _dbCommand );
            var entry = ResultCache.Instance[ hashedKey ];
            if ( entry == null )
            {
                result = _dbCommand.ExecuteScalar( );
                Logger.TraceInformation("ExecuteScalar(): {0} --> result: {1}", _dbCommand.CommandText, result);
                ResultCache.Instance.Add(hashedKey, result, _dbCommand.CommandText, DbParameterCollectionToString(_dbCommand.Parameters));
            }
            else
            {
                result = entry.ScalarResult;
            }

            return result;
        }

        public override void Prepare( )
        {
            _dbCommand.Prepare( );
        }

        #endregion

        #region Private Methods

        protected override DbConnection DbConnection
        {
            get {
                return ConfigurationHelper.Instance.MockMode ? new MockConnection( ) : _dbCommand.Connection;
            }
            set
            {
                var castValue = value as MockConnection;
                _dbCommand.Connection = castValue == null ? value : castValue.RealConnection;
            }
        }

        protected override DbParameterCollection DbParameterCollection
        {
            get
            {
                return _dbCommand.Parameters;
            }
        }

        protected override DbTransaction DbTransaction
        {
            get {
                return ConfigurationHelper.Instance.MockMode ? _dbTransaction : _dbCommand.Transaction;
            }
            set
            {
                if ( ConfigurationHelper.Instance.MockMode )
                    _dbTransaction = value;
                else
                    _dbCommand.Transaction = value;
            }
        }

        protected override DbParameter CreateDbParameter( )
        {
            return _dbCommand.CreateParameter( );
        }

        protected override DbDataReader ExecuteDbDataReader( CommandBehavior behavior )
        {
            DbDataReader result;

            var hashedKey = ResultCache.GetHashedKey( ResultCache.CacheEntryType.ExecuteDbDataReader.ToString( ), _dbCommand );
            var entry = ResultCache.Instance[ hashedKey ];
            if ( entry == null )
            {
                result = _dbCommand.ExecuteReader( behavior );
                var recordsAffected = result.RecordsAffected;

                var ds = new DataSet( );
                DataReaderConverter.FillDataSetFromReader( ds, result );

                Logger.TraceInformation("ExecuteDbDataReader(): {0} --> result.RecordsAffected: {1}", _dbCommand.CommandText, recordsAffected);
                ResultCache.Instance.Add(hashedKey, recordsAffected, ds, _dbCommand.CommandText, DbParameterCollectionToString(_dbCommand.Parameters));

                result.Close( );
                result.Dispose( );

                result = new MockDataReader( ds, recordsAffected );
            }
            else
            {
                result = new MockDataReader( entry.DataSetResult, ( int )entry.ScalarResult );
            }

            return result;
        }

        private static string DbParameterCollectionToString(DbParameterCollection coll)
        {
            if (coll == null) throw new ArgumentNullException("coll");

            var ret = new StringBuilder();
            foreach (var item in coll)
            {
                ret.Append((item as DbParameter).ParameterName).Append(';');
            }
            return ret.ToString();
        }

        #endregion
    }
}