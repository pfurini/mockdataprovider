﻿using System.Diagnostics;

namespace MockDataProvider
{
    internal static class LogFactory
    {
        internal static TraceSource NewTraceSource()
        {
            return new TraceSource("MockDataProvider", SourceLevels.Error);
        }
    }
}
