using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Xml;

namespace MockDataProvider.Configuration
{
    internal sealed class SectionHandler : IConfigurationSectionHandler
    {
        #region IConfigurationSectionHandler Members

        public object Create( object parent, object configContext, XmlNode section )
        {
            if ( section == null )
                throw new ConfigurationErrorsException( "Mock ADO.NET Data Provider configuration section not found" );

            #region General Configuration

            var realDataProvider = String.Empty;
            if ( section.Attributes[ "realDataProvider" ] != null && !String.IsNullOrEmpty( section.Attributes[ "realDataProvider" ].Value ) )
                realDataProvider = section.Attributes[ "realDataProvider" ].Value;

            var cacheFile = String.Empty;
            if ( section.Attributes[ "cacheFile" ] != null && !String.IsNullOrEmpty( section.Attributes[ "cacheFile" ].Value ) )
                cacheFile = section.Attributes[ "cacheFile" ].Value;

            var mockMode = true;
            if ( section.Attributes[ "mockMode" ] != null )
                mockMode = Convert.ToBoolean( section.Attributes[ "mockMode" ].Value, CultureInfo.InvariantCulture );

            var ignoreDateTimeFields = false;
            if (section.Attributes["ignoreDateTimeFields"] != null)
                ignoreDateTimeFields = Convert.ToBoolean(section.Attributes["ignoreDateTimeFields"].Value, CultureInfo.InvariantCulture);

            var parmsToIgnore = String.Empty;
            if (section.Attributes["parametersToIgnore"] != null && !String.IsNullOrEmpty(section.Attributes["parametersToIgnore"].Value))
                parmsToIgnore = section.Attributes["parametersToIgnore"].Value;
            #endregion

            return new ConfigurationInformation( realDataProvider, cacheFile, mockMode, ignoreDateTimeFields, parmsToIgnore );
        }

        #endregion
    }
}