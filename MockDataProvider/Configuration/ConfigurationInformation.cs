using System;
using System.Collections.Generic;

namespace MockDataProvider.Configuration
{
    internal sealed class ConfigurationInformation
    {
        #region Constructors

        public ConfigurationInformation( string realDataProvider, string cacheFile, bool mockMode, bool ignoreDateTimeFields, string parametersToIgnore)
        {
            _realDataProvider = realDataProvider;
            _cacheFile = cacheFile;
            _mockMode = mockMode;
            _ignoreDateTimeFields = ignoreDateTimeFields;
            _parametersToIgnore = parametersToIgnore;
        }

        #endregion

        #region Properties

        private readonly string _realDataProvider;

        public string RealDataProvider
        {
            get
            {
                return _realDataProvider;
            }
        }

        private readonly string _cacheFile;

        public string CacheFile
        {
            get
            {
                return _cacheFile;
            }
        }

        private readonly bool _mockMode;

        public bool MockMode
        {
            get
            {
                return _mockMode;
            }
        }

        public bool IgnoreDateTimeFields
        {
            get { return _ignoreDateTimeFields; }
        }

        private readonly bool _ignoreDateTimeFields;


        private readonly string _parametersToIgnore;

        public string ParametersToIgnore
        {
            get
            {
                return _parametersToIgnore;
            }
        }
        #endregion
    }
}