using System;
using System.Collections.Generic;
using System.Configuration;

namespace MockDataProvider.Configuration
{
    public class ConfigurationHelper
    {
        #region Factory

        public static readonly ConfigurationHelper Instance = new ConfigurationHelper();

        #endregion

        #region Private Vars

        private readonly ConfigurationInformation _config;
        private static readonly Dictionary<string, string> _substVars = new Dictionary<string, string>();

        #endregion

        #region Constructors

        private ConfigurationHelper( )
        {
            _config = ConfigurationManager.GetSection( "MockDataProvider" ) as ConfigurationInformation;
        }

        #endregion

        #region Properties

        public string RealDataProvider
        {
            get
            {
                return _config.RealDataProvider;
            }
        }

        public string CacheFile
        {
            get
            {
                //TODO: implement caching of transformed value with dictionary change notification
                var res = _config.CacheFile;
                foreach (var @var in _substVars)
                {
                    res = res.Replace(@var.Key, @var.Value);
                }
                return res;
            }
        }

        public bool MockMode
        {
            get
            {
                return _config.MockMode;
            }
        }

        public bool IgnoreDateTimeFields
        {
            get { return _config.IgnoreDateTimeFields; }
        }

        public string ParametersToIgnore
        {
            get
            {
                return _config.ParametersToIgnore;
            }
        }

        #endregion

        #region Methods
        public static void AddOrUpdateSubstitutionVar(string key, string value)
        {
            if (key == null) throw new ArgumentNullException("key");
            if (value == null) throw new ArgumentNullException("value");
            _substVars['|' + key + '|'] = value;
        }
        public static void RemoveSubstitutionVar(string key)
        {
            if (key == null) throw new ArgumentNullException("key");
            _substVars.Remove('|' + key + '|');
        }

        #endregion
    }
}