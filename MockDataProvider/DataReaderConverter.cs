using System;
using System.Data;
using System.Data.Common;

namespace MockDataProvider
{
    internal class DataReaderConverter : DbDataAdapter
    {
        public int FillFromReader( DataTable data, IDataReader dataReader )
        {
            return this.Fill( data, dataReader );
        }

        protected override RowUpdatedEventArgs CreateRowUpdatedEvent( DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping )
        {
            return null;
        }

        protected override RowUpdatingEventArgs CreateRowUpdatingEvent( DataRow dataRow, IDbCommand command, StatementType statementType, DataTableMapping tableMapping )
        {
            return null;
        }

        protected override void OnRowUpdated( RowUpdatedEventArgs value )
        {
        }

        protected override void OnRowUpdating( RowUpdatingEventArgs value )
        {
        }

        public static void FillDataSetFromReader( DataSet data, IDataReader dataReader )
        {
            DataReaderConverter converter = new DataReaderConverter( );

            do
            {
                DataTable table = new DataTable( );
                converter.FillFromReader( table, dataReader );
                data.Tables.Add( table );
            }
            while ( dataReader.NextResult( ) );
        }
    }
}