using System;
using System.Data;
using System.Data.Common;
using System.Security;
using System.Security.Permissions;

namespace MockDataProvider
{
    public class MockFactory : DbProviderFactory
    {
        #region Factory

        public static readonly MockFactory Instance = new MockFactory( );

        #endregion

        #region Constructors

        public MockFactory( ) { }

        #endregion
        
        #region Factory

        public override bool CanCreateDataSourceEnumerator
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region Public Methods

        public override DbCommand CreateCommand( )
        {
            return new MockCommand( );
        }

        public override DbCommandBuilder CreateCommandBuilder( )
        {
            return RealProviderFactory.Instance.CreateCommandBuilder( );
        }

        public override DbConnection CreateConnection( )
        {
            return new MockConnection( );
        }

        public override DbConnectionStringBuilder CreateConnectionStringBuilder( )
        {
            return RealProviderFactory.Instance.CreateConnectionStringBuilder( );
        }

        public override DbDataAdapter CreateDataAdapter( )
        {
            return new MockDataAdapter( );
        }

        public override DbDataSourceEnumerator CreateDataSourceEnumerator( )
        {
            return RealProviderFactory.Instance.CreateDataSourceEnumerator( );
        }

        public override DbParameter CreateParameter( )
        {
            return RealProviderFactory.Instance.CreateParameter( );
        }

        public override CodeAccessPermission CreatePermission( PermissionState state )
        {
            return RealProviderFactory.Instance.CreatePermission( state );
        }

        #endregion
    }
}