﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyTitle( "Unit Test Tools Sample Unit Tests" )]
[assembly: AssemblyDescription( "Unit Test Tools Sample Unit Tests" )]

[assembly: ComVisible( false )]

[assembly: Guid( "4436c966-0939-480c-ab5b-b2280082c28d" )]

[assembly: CLSCompliant( true )]

[assembly: EnvironmentPermissionAttribute( SecurityAction.RequestMinimum, Read = "COMPUTERNAME;USERNAME;USERDOMAIN" )]

[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "Lagash Systems" )]
[assembly: AssemblyProduct( "Unit Test Tools Sample Unit Tests" )]
[assembly: AssemblyCopyright( "Rodolfo Finochietti" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

[assembly: AssemblyVersion( "1.0.3.0" )]
[assembly: AssemblyFileVersion( "1.0.3.0" )]
[assembly: AssemblyInformationalVersion( "1.0.3.0" )]
