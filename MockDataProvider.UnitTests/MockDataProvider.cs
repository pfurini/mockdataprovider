using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Transactions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MockProvider.UnitTests
{
    [TestClass]
    public class MockDataProvider
    {
        public MockDataProvider( ) { }

        [TestMethod]
        [Owner( "Rodolfo Finochietti" )]
        [DeploymentItem( "Cache.xml" )]
        public void WithoutTransactionTest( )
        {
            Run( false );
        }

        [TestMethod]
        [Owner( "Rodolfo Finochietti" )]
        [DeploymentItem( "Cache.xml" )]
        public void WithAdoTransactionTest( )
        {
            Run( true );
        }

        [TestMethod]
        [Owner( "Rodolfo Finochietti" )]
        [DeploymentItem( "Cache.xml" )]
        public void WithSystemTransactionTest( )
        {
            using ( TransactionScope ts = new TransactionScope( ) )
            {
                Run( false );
            }
        }

        public void Run( bool createAdoTransaction )
        {
            DbProviderFactory dataFactory = DbProviderFactories.GetFactory( "MockDataProvider" );

            using ( DbConnection connection = dataFactory.CreateConnection( ) )
            {
                connection.ConnectionString = ConfigurationManager.ConnectionStrings[ "MockDataProviderTest" ].ConnectionString;
                connection.Open( );

                DbTransaction transaction = null;
                if ( createAdoTransaction )
                    transaction = connection.BeginTransaction( );

                DbCommand selectCommand = connection.CreateCommand( );
                selectCommand.Transaction = transaction;
                selectCommand.CommandText = "SELECT * FROM Currency WHERE CurrencyCode = @CurrencyCode";

                DbParameter paramSelectCurrencyCode = dataFactory.CreateParameter( );
                paramSelectCurrencyCode.ParameterName = "@CurrencyCode";
                paramSelectCurrencyCode.Value = "XXX";
                selectCommand.Parameters.Add( paramSelectCurrencyCode );

                Assert.AreEqual( "XXX", selectCommand.ExecuteScalar( ), "ExecuteScalar" );

                DbCommand insertCommand = dataFactory.CreateCommand( );
                insertCommand.Connection = connection;
                insertCommand.Transaction = transaction;
                insertCommand.CommandText = "INSERT INTO Currency(Name, CurrencyCode) VALUES (@Name, @CurrencyCode)";

                DbParameter paramInsertName = insertCommand.CreateParameter( );
                paramInsertName.ParameterName = "@Name";
                paramInsertName.SourceColumn = "Name";
                insertCommand.Parameters.Add( paramInsertName );

                DbParameter paramInsertCurrencyCode = insertCommand.CreateParameter( );
                paramInsertCurrencyCode.ParameterName = "@CurrencyCode";
                paramInsertCurrencyCode.SourceColumn = "CurrencyCode";
                insertCommand.Parameters.Add( paramInsertCurrencyCode );


                DbCommand updateCommand = dataFactory.CreateCommand( );
                updateCommand.Connection = connection;
                updateCommand.Transaction = transaction;
                updateCommand.CommandText = "UPDATE Currency SET Name = @Name WHERE CurrencyCode = @CurrencyCode";

                DbParameter paramUpdateName = updateCommand.CreateParameter( );
                paramUpdateName.ParameterName = "@Name";
                paramUpdateName.SourceColumn = "Name";
                updateCommand.Parameters.Add( paramUpdateName );

                DbParameter paramUpdateCurrencyCode = updateCommand.CreateParameter( );
                paramUpdateCurrencyCode.ParameterName = "@CurrencyCode";
                paramUpdateCurrencyCode.SourceColumn = "CurrencyCode";
                updateCommand.Parameters.Add( paramUpdateCurrencyCode );


                DbCommand deleteCommand = dataFactory.CreateCommand( );
                deleteCommand.Connection = connection;
                deleteCommand.Transaction = transaction;
                deleteCommand.CommandText = "DELETE FROM Currency WHERE CurrencyCode = @CurrencyCode";

                DbParameter paramDelereCurrencyCode = deleteCommand.CreateParameter( );
                paramDelereCurrencyCode.ParameterName = "@CurrencyCode";
                paramDelereCurrencyCode.SourceColumn = "CurrencyCode";
                deleteCommand.Parameters.Add( paramDelereCurrencyCode );

                DataSet ds = new DataSet( );
                DataTable dt = new DataTable( );

                DbDataAdapter adapter = dataFactory.CreateDataAdapter( );
                adapter.SelectCommand = selectCommand;
                adapter.UpdateCommand = updateCommand;
                adapter.InsertCommand = insertCommand;
                adapter.DeleteCommand = deleteCommand;

                adapter.Fill( ds );
                Assert.AreEqual( 1, ds.Tables[ 0 ].Rows.Count, "DataSet Count" );

                adapter.Fill( dt );
                Assert.AreEqual( 1, dt.Rows.Count, "DataTable Count" );

                ds.Tables[ 0 ].Rows[ 0 ].Delete( );
                Assert.AreEqual( 1, adapter.Update( ds ), "Delete" );

                DataRow row = ds.Tables[ 0 ].NewRow( );
                row[ 0 ] = "XXX";
                row[ 1 ] = "XXX";
                ds.Tables[ 0 ].Rows.Add( row );
                Assert.AreEqual( 1, adapter.Update( ds, ds.Tables[ 0 ].TableName ), "Insert" );

                ds.Tables[ 0 ].Rows[ 0 ][ 1 ] = "Test";
                Assert.AreEqual( 1, adapter.Update( ds ), "Update" );

                DbDataReader reader = selectCommand.ExecuteReader( );

                int i = 0;
                while ( reader.Read( ) )
                    i++;

                reader.Close( );

                Assert.AreEqual( 1, i, "Reader" );

                DataSet multipleResults = new DataSet( );

                DbCommand multipleResultsCommand = connection.CreateCommand( );
                multipleResultsCommand.Transaction = transaction;
                multipleResultsCommand.CommandText = "GetMutipleResults";
                multipleResultsCommand.CommandType = CommandType.StoredProcedure;
                
                DbDataAdapter multipleResultsAdapter = dataFactory.CreateDataAdapter( );
                multipleResultsAdapter.SelectCommand = multipleResultsCommand;

                multipleResultsAdapter.Fill( multipleResults );
                Assert.AreEqual( 2, multipleResults.Tables.Count, "Multiple Results Tables Count" );
                Assert.IsTrue( multipleResults.Tables[ 0 ].Rows.Count == 10, "Multiple Results Table 1 Rows Count" );
                Assert.IsTrue( multipleResults.Tables[ 1 ].Rows.Count == 10, "Multiple Results Table 2 Rows Count" );
               
                if ( createAdoTransaction )
                    transaction.Commit( );
            }
        }
    }
}